# List of Bitbucket emojis
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

## Usage
Use it in
- commit messages
- markdown

## Emoji list
| Tables                      | Syntax                      |
|----------------------------:|:----------------------------|
| :arrow_down:                | `:arrow_down:`              |
| :arrow_left:                | `:arrow_left:`              |
| :arrow_lower_left:          | `:arrow_lower_left:`        |
| :arrow_lower_right:         | `:arrow_lower_right:`       |
| :arrow_right:               | `:arrow_right:`             |
| :arrow_up:                  | `:arrow_up:`                |
| :arrow_upper_left:          | `:arrow_upper_left:`        |
| :arrow_upper_right:         | `:arrow_upper_right:`       |
| :arrows_counterclockwise:   | `:arrows_counterclockwise:` |
| :art:                       | `:art:`                     |
| :beetle:                    | `:beetle:`                  |
| :bell:                      | `:bell:`                    |
| :calendar:                  | `:calendar:`                |
| :car:                       | `:car:`                     |
| :cloud:                     | `:cloud:`                   |
| :diamonds:                  | `:diamonds:`                |
| :email:                     | `:email:`                   |
| :envelope_with_arrow:       | `:envelope_with_arrow:`     |
| :file_folder:               | `:file_folder:`             |
| :fire:                      | `:fire:`                    |
| :gem:                       | `:gem:`                     |
| :grinning:                  | `:grinning:`                |



Collected and summarized by [merukeru](http://merukeru.bitbucket.org/)